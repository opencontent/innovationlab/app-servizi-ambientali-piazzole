import { Component, ElementRef, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import L, { icon, LatLngExpression, Map, Marker, marker, tileLayer } from 'leaflet';
import { Geolocation } from '@capacitor/geolocation';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';
import { ZoneService } from '../../services/zone.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {


  @ViewChild('map', { read: ElementRef, static: true }) mapElement: ElementRef;

  @Input() coords: any;
  @Input() address: string;
  private geocode: LatLngExpression
  private watchId: any;
  private marker: Marker;
  private mapRef: Map;
  private watchCoordinate: { latitude: any; longitude: any };
  private coordinate: { latitude: number; accuracy: number; longitude: number };
  private label: string;
  InitPosition: any = {
    lat: 44.4471372,
    lng: 8.7507472,
  };

  constructor(
    public modalController: ModalController,
    public ngZone: NgZone,
    private zoneService: ZoneService,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {

  }

  ngOnInit() {
    this.presentLoading();
  }

  ionViewDidEnter() {}

  initMap() {
    const map = new Map(this.mapElement.nativeElement).setView(this.geocode, 20);
    this.mapRef = map;
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    const customMarkerIcon = icon({
      iconUrl: 'assets/icon/marker.png',
      iconSize: [40, 102],
      popupAnchor: [0, -40],
    });

    this.updateLabelMarker(this.InitPosition);

    this.marker = marker(this.geocode, { icon: customMarkerIcon, draggable: true })
      .bindPopup(`<b>${this.label}</b>`, { autoClose: false })
      .on('click', () => {})
      .on('dragend', (event) => {
        const marker = event.target;
        const position = marker.getLatLng();
        this.updateLabelMarker(position);
      })
      .addTo(map);
    // .openPopup();

    this.centerLeafletMapOnMarker(this.mapRef, this.marker);
  }

  dismiss(type: boolean) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    if (type === true) {
      this.modalController.dismiss({
        dismissed: true,
        geocode: this.geocode,
        address: this.label,
      });
    } else {
      this.modalController.dismiss({
        dismissed: true,
      });
    }
  }

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [marker.getLatLng()];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  }

  updateLabelMarker(position: any) {
    this.zoneService.addressLookup([position.lat, position.lng]).subscribe((res) => {
      let data = res.replace(/cb\(/g, '');
      const jsonData = JSON.parse(data.replace(/\)/g, ''));
      this.label = jsonData.display_name;
      this.geocode = [position.lat, position.lng];
      this.marker.setPopupContent(`<b>${jsonData.display_name}</b>`).openPopup();
    });
  }

  getCurrentCoordinate() {
    if (!Capacitor.isPluginAvailable('Geolocation')) {
      console.log('Plugin geolocation not available');
      return;
    }
    Geolocation.getCurrentPosition()
      .then((data) => {
        this.coordinate = {
          latitude: data.coords.latitude,
          longitude: data.coords.longitude,
          accuracy: data.coords.accuracy,
        };
      })
      .catch((err) => {
        console.error(err);
      });
  }

  watchPosition() {
    try {
      this.watchId = Geolocation.watchPosition({}, (position, err) => {
        this.ngZone.run(() => {
          this.watchCoordinate = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
        });
      });
    } catch (e) {
      console.error(e);
    }
  }

  clearWatch() {
    if (this.watchId != null) {
      Geolocation.clearWatch({ id: this.watchId });
    }
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    const checkPermission = await Geolocation.checkPermissions();
    console.log('Current', coordinates);
    console.log('checkPermission', checkPermission);
    return coordinates;
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Caricamento...',
    });
    debugger
    this.InitPosition = {
      lat: this.coords[0],
      lng:this.coords[1]
    };
    this.geocode = this.coords
    this.initMap();
   /* await loading.present().then(() => {
      this.getCurrentPosition()
        .then((r) => {
          this.zoneService.addressLookup([r.coords.latitude, r.coords.longitude]).subscribe(
            (res) => {
              let data = res.replace(/cb\(/g, '');
              const jsonData = JSON.parse(data.replace(/\)/g, ''));
              this.label = jsonData.display_name;
              this.geocode = [r.coords.latitude, r.coords.longitude];
              this.initMap();
            },
            (error) => {
              loading.dismiss();
            },
            () => {
              loading.dismiss();
            }
          );
        })
        .catch((err) => {
          loading.dismiss();
          this.presentToastWithOptions();
          this.initMap();
        });
    });*/

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Geocalizazzione non abilitata',
      message: '',
      position: 'top',
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
