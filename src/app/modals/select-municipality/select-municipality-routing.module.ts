import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectMunicipalityPage } from './select-municipality.page';

const routes: Routes = [
  {
    path: '',
    component: SelectMunicipalityPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectMunicipalityPageRoutingModule {}
