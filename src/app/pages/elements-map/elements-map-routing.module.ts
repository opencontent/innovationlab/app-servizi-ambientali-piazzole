import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ElementsMapPage} from "./elements-map.page";


const routes: Routes = [
  {
    path: '',
    component: ElementsMapPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EcoPointsDetailsPageRoutingModule {}
