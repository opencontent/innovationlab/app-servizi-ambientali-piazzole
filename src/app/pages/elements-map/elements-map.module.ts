import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EcoPointsDetailsPageRoutingModule } from './elements-map-routing.module';
import {ElementsMapPage} from "./elements-map.page";

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, EcoPointsDetailsPageRoutingModule ],
  declarations: [ElementsMapPage],
})
export class EcoPointsDetailsPageModule {}
