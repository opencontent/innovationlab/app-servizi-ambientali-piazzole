import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  selectedIndex: any = 0;
  loading: false;
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  public selectedSection = 'home';
  token: string;
  municipalityData: any;
  counter: number;
  constructor(private router: Router, private storage: StorageService) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  segmentChosen(segment: string) {
    this.selectedSection = segment;
  }

  logout() {
    this.removeToken().then(() => {
      window.location.reload();
    });
  }

  async removeToken() {
    await this.storage.removeToken();
  }

  updateMunicipality(newItem: any) {
    this.municipalityData = newItem;
  }
}
