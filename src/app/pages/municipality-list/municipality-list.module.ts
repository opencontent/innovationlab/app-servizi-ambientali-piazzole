import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MunicipalityListPageRoutingModule } from './municipality-list-routing.module';

import { MunicipalityListPage } from './municipality-list.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, MunicipalityListPageRoutingModule],
  declarations: [MunicipalityListPage],
  exports: [MunicipalityListPage],
})
export class MunicipalityListPageModule {}
