import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportsService } from '../../services/reports.service';
import { StorageService } from '../../services/storage.service';
import { ZoneService } from '../../services/zone.service';
import { fiscalCodeValidator } from '../../directives/fiscal-code-validator.directive';
import { takeUntil } from 'rxjs/operators';
import { MapPage } from '../../modals/map/map.page';
import { Camera, CameraResultType } from '@capacitor/camera';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-new-post-elements',
  templateUrl: './new-post-elements.page.html',
  styleUrls: ['./new-post-elements.page.scss'],
})
export class NewPostElementsPage implements OnInit, OnDestroy {
  comuni: any[];
  categories: any[];
  types: any[];
  loading = true;
  error: any;
  token: string;
  bookingForm: FormGroup;
  isSubmitted = false;
  private unsubscribe$ = new Subject<void>();
  lat: any;
  lng: any;
  watchId: any;
  items = [];
  userData: any;
  imageElements: any[] = [];
  dataDetails: any;
  private address: string;

  constructor(
    private apollo: Apollo,
    public alertController: AlertController,
    private router: Router,
    private serviceReport: ReportsService,
    public formBuilder: FormBuilder,
    private storage: StorageService,
    private zoneService: ZoneService,
    public modalController: ModalController,
    public ngZone: NgZone,
    public dms: DomSanitizer,
    public loadingController: LoadingController,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.dataDetails = this.router.getCurrentNavigation().extras.state.dataDetails;
        this.address  = this.router.getCurrentNavigation().extras.state.address;
      }
    });
  }

  get errorControl() {
    return this.bookingForm.controls;
  }

  ngOnInit() {
    this.bookingForm = this.formBuilder.group({
      full_name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      category: [''],
      type: ['', [Validators.required]],
      address: [''],
      description: ['',[Validators.required]],
      geocode: [''],
      images: [''],
      fiscal_code: [
        null,
        [
          Validators.required,
          fiscalCodeValidator(/^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$/),
        ],
      ],
    });

    //Set data elements
    this.bookingForm.controls['address'].setValue(this.address);
    this.bookingForm.controls['geocode'].setValue(this.dataDetails.point);

    this.storage.getUserData().then((res) => {
      if (res) {
        this.userData = JSON.parse(res);
        this.populateFormUser(this.userData);
        this.bookingForm.controls['full_name'].setValue(this.userData.data_user);
      }
    });

    this.zoneService
      .getAllZone()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.comuni = res.items;
      });

    this.zoneService
      .getCategories()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.items && res.items.length > 0) {
          this.categories = res.items
        }
      });

    this.zoneService
      .getTypes()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.items && res.items.length > 0) {
          this.types = res.items;
        }
      });

    function filterByValue(array, property, string) {
      return array.filter((o) => Object.keys(o).some((k) => o[property].toLowerCase().includes(string.toLowerCase())));
    }

    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.presentAlertConfirm();
      }
    });
  }

  async onSubmit() {
    this.isSubmitted = true;
    if (!this.bookingForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Invio segnalazione in corso...',
      });
      await loading.present().then(() => {
        this.serviceReport
          .createPublicPost(this.bookingForm.value,this.dataDetails)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.presentAlertPost(
                'Segnalazione inviata con successo',
                'Riceverai al più presto informazioni sullo stato della segnalazione'
              );
              this.bookingForm.reset();
              this.bookingForm.clearValidators();
              this.router.navigate(['posts']);
            },
            (error) => {
              this.presentAlertPost('Opss!', 'Ci sono stati dei problemi, riprova più tardi!');
            },
            () => {
              loading.dismiss();
            }
          );
      });
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Non sei abilitato a questa sezione',
      message:
        'Per effettuare una prenotazione devi essere <strong>registrato</strong> <br> effettua il login e riprova',
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.router.navigate(['home']);
          },
        },
        {
          text: 'Login',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
      ],
    });

    await alert.present();
  }

  async presentAlertPost(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Chiudi'],
    });

    await alert.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: MapPage,
      cssClass: '',
      componentProps: {
        address: this.address,
        coords: this.dataDetails.point
      }
    });
    modal.onDidDismiss().then((data) => {
      this.bookingForm.controls['address'].setValue(data.data.address);
      this.bookingForm.controls['geocode'].setValue(data.data.geocode);
    });
    return await modal.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  populateFormUser(user: any) {
    if (this.userData) {
      var data_user = {
        full_name: this.userData.data_user.name,
        phone: this.userData.data_user.phone_number,
        fiscal_code: this.userData.data_user.fiscal_code,
      };
      this.bookingForm.patchValue(data_user);
    }
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Base64,
      saveToGallery: true,
      correctOrientation: true,
      height: 1024,
      width: 768,
      promptLabelPicture: 'Scatta foto',
      promptLabelPhoto: 'Carica da galleria',
      promptLabelCancel: 'Chiudi',
      promptLabelHeader: 'Carica foto',
    });
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    // Can be set to the src of an image now
    let img = {
      file: image.base64String,
      filename: 'img-' + Date.now(),
    };
    if (this.imageElements.length < 3) {
      this.imageElements.push(img);
      this.bookingForm.controls['images'].setValue(this.imageElements);
    } else {
      this.presentAlertPost('Attenzione', 'Numero massimo di immagini caricabili raggiunto');
    }
  }

  display(b64: string) {
    return this.dms.bypassSecurityTrustUrl('data:image/jpeg;base64,' + b64);
  }

  deletePhoto(index: number) {
    this.imageElements.splice(index, 1);
    this.bookingForm.controls['images'].setValue(this.imageElements);
  }
}
