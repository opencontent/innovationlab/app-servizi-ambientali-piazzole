import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private _storage: Storage | null = null;

  constructor(private storage: Storage) {
    this.init();
  }

  async init() {
    // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    const storage = await this.storage.create();
    this._storage = storage;
  }

  // Create and expose methods that users of this service can
  // call, for example:
  public set(key: string, value: any) {
    return this._storage?.set(key, value);
  }
  public get(key: string) {
    return this._storage?.get(key);
  }

  public getMunicipality() {
    return this._storage?.get('municipality');
  }

  public getToken() {
    return this._storage?.get('ACCESS_TOKEN');
  }

  public getUserData() {
    return this._storage?.get('DATA_USER');
  }

  public getUserRole() {
    return this._storage?.get('USER_ROLE');
  }
  public removeToken() {
    return this._storage?.remove('ACCESS_TOKEN');
  }

  public clearStorage() {
    return this._storage?.clear();
  }
}
